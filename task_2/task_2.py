from sqlalchemy.orm import declarative_base, sessionmaker
from sqlalchemy import (Column, Integer, String, Float, Date, ForeignKey,
                        Table, create_engine)
from sqlalchemy.orm import relationship
from sqlalchemy.exc import IntegrityError
import os
import json
from datetime import date


Base = declarative_base()


content_country = Table(
     'content_country',
     Base.metadata,
     Column('content_id', ForeignKey('contents.content_id'), primary_key=True),
     Column('country_id', ForeignKey('countries.country_id'), primary_key=True)
)


class Content(Base):
    __tablename__ = 'contents'

    content_id = Column(String(40), primary_key=True)
    duration = Column(Float, unique=False, nullable=True)
    form_id = Column(Integer, ForeignKey('forms.form_id'), nullable=True)
    original_lang_id = Column(Integer, ForeignKey('languages.lang_id'),
                              nullable=True)
    year = Column(Date, unique=False, nullable=True)

    countries = relationship('Country', secondary=content_country,
                             back_populates='contents')

    form = relationship('Form', back_populates='content')

    original_lang = relationship('Language', back_populates='content')


class Country(Base):
    __tablename__ = 'countries'

    country_id = Column(Integer, primary_key=True)
    country = Column(String, unique=True, nullable=False)

    contents = relationship('Content', secondary=content_country,
                            back_populates='countries')


class Form(Base):
    __tablename__ = 'forms'

    form_id = Column(Integer, primary_key=True)
    form = Column(String, unique=True, nullable=False)


Form.content = relationship('Content', back_populates='form')


class Text(Base):
    __tablename__ = 'texts'

    id = Column(Integer, primary_key=True)  # this is due to duplicate text_id values with different text_len
    text_id = Column(String(40), unique=False, nullable=False)
    lang_id = Column(Integer, ForeignKey('languages.lang_id'), nullable=True)
    text_len = Column(Integer, unique=False, nullable=True)
    content_id = Column(String(40), ForeignKey('contents.content_id'))

    content = relationship('Content', back_populates='texts')

    lang = relationship('Language', back_populates='text')


Content.texts = relationship('Text', order_by=Text.id,
                             back_populates='content')


class Language(Base):
    __tablename__ = 'languages'

    lang_id = Column(Integer, primary_key=True)
    lang = Column(String, unique=True, nullable=False)


Language.content = relationship('Content', back_populates='original_lang')
Language.text = relationship('Text', back_populates='lang')


pg_user = os.getenv('PG_USER')
pg_pass = os.getenv('PG_PASS')
pg_host = os.getenv('PG_HOST')
pg_db = os.getenv('PG_DB')

database_url = f'postgresql://{pg_user}:{pg_pass}@{pg_host}/{pg_db}'
engine = create_engine(database_url)
Base.metadata.create_all(engine)

Session = sessionmaker(bind=engine)
session = Session()

with open('data.json') as f:
    data = json.load(f)

for item in data['contents']:

    form = Form(form=item['form'])
    try:
        session.add(form)
        session.commit()
    except IntegrityError:
        session.rollback()
        form = session.query(Form). \
            filter(Form.form.in_([item['form']])).first()

    original_lang = Language(lang=item['original_lang'])
    try:
        session.add(original_lang)
        session.commit()
    except IntegrityError:
        session.rollback()
        original_lang = session.query(Language). \
            filter(Language.lang.in_([item['original_lang']])).first()

    # TODO: this assumes we want to get rid of duplicate countries
    country_names = [country['country'] for country in item['countries']]
    country_names = list(set(country_names))
    countries = []
    for country_name in country_names:
        country = Country(country=country_name)
        try:
            session.add(country)
            session.commit()
        except IntegrityError:
            session.rollback()
            country = session.query(Country). \
                filter(Country.country.in_([country_name])).first()
        countries.append(country)

    content = Content(content_id=item['content_id'],
                      duration=item['duration'],
                      form_id=getattr(form, 'form_id', None),
                      original_lang_id=getattr(original_lang, 'lang_id', None),
                      year=date(year=item['year'], month=1, day=1) if item['year'] is not None else None
                      )
    session.add(content)
    session.commit()

    for country in countries:
        content.countries.append(country)
        session.commit()
    country.contents.append(content)

    for text_dict in item['texts']:
        lang = Language(lang=text_dict['lang'])
        try:
            session.add(lang)
            session.commit()
        except IntegrityError:
            session.rollback()
            lang = session.query(Language). \
                filter(Language.lang.in_([text_dict['lang']])).first()

        text = Text(text_id=text_dict['text_id'],
                    lang_id=getattr(lang, 'lang_id', None),
                    text_len=text_dict['text_len'],
                    content_id=content.content_id
                    )
        session.add(text)
        session.commit()

session.commit()
